/*global $ */
/*eslint-env browser*/
    function goFullscreen(id) {
      var element = document.getElementById(id);       
      if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
          element.play();
      } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
          element.play();
      }   else if (element.RequestFullScreen) {
        element.webkitRequestFullScreen();
          element.play();
      }  
    }
$(document).ready(function () {
    'use strict';
    $('.sliderHome .slider').slick({
        arrows: false,
        dots:true,
        rtl:true,
        autoplay: true,
        autoplaySpeed: 2000,
    });
    $('.videos  .sm-v video').click( function (){
        var geId = $(this).attr('id');
        goFullscreen(geId);
    })
    $('.videos  .videoSlider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow:".sliderControl .arrows.prev",
        nextArrow:".sliderControl .arrows.next",
        rtl:true,
        dots:false
        
    });

	
});
//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});
$(document).ready(function () {
    'use strict';
    $(window).click(function() {
        $(".collapse").removeClass("show");
    });
    
    $('.collapse ').click(function(event){
        event.stopPropagation();
    });
     $("nav li a").click(function (e){

           e.preventDefault();
            $("html, body").animate({
                scrollTop: $("#"+ $(this).data("scroll")).offset().top
            }, 1000);
          //Add Class Active
//          $(this).addClass("active").parent().siblings().find("a").removeClass("active");
       });
        // Sycn Navbar
    $("footer .mainF li a").click(function (e){

           e.preventDefault();
            $("html, body").animate({
                scrollTop: $("#"+ $(this).data("scroll")).offset().top
            }, 1000);
       });
        $(window).scroll(function (){
            $("section").each(function (){
                if($(window).scrollTop() > ($(this).offset().top - 50)){
                    var sectionID=$(this).attr("id");
                    $("nav li a ").removeClass("active");
                    $("nav li a[data-scroll='"+ sectionID +"']").addClass("active");
                }
            })
        })
});
